package ru.kuzin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.ProjectStartByIdRequest;
import ru.kuzin.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setId(id);
        getProjectEndpoint().startProjectById(request);
    }

}