package ru.kuzin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public TaskListRequest(@Nullable final Sort sort) {
        this.sortType = sortType;
    }

    public TaskListRequest(@Nullable final String token) {
        super(token);
    }

}